

# #########################################

"""
Monty Hall Problem
"""

import pomegranate as pom
import networkx as nx

guest = pom.DiscreteDistribution({'A': 1/3, 'B': 1/3, 'C':1/3})
prize = pom.DiscreteDistribution({'A': 1./3, 'B': 1./3, 'C': 1./3})

monty = pom.ConditionalProbabilityTable(
        [['A', 'A', 'A', 0.0],
         ['A', 'A', 'B', 0.5],
         ['A', 'A', 'C', 0.5],
         ['A', 'B', 'A', 0.0],
         ['A', 'B', 'B', 0.0],
         ['A', 'B', 'C', 1.0],
         ['A', 'C', 'A', 0.0],
         ['A', 'C', 'B', 1.0],
         ['A', 'C', 'C', 0.0],
         ['B', 'A', 'A', 0.0],
         ['B', 'A', 'B', 0.0],
         ['B', 'A', 'C', 1.0],
         ['B', 'B', 'A', 0.5],
         ['B', 'B', 'B', 0.0],
         ['B', 'B', 'C', 0.5],
         ['B', 'C', 'A', 1.0],
         ['B', 'C', 'B', 0.0],
         ['B', 'C', 'C', 0.0],
         ['C', 'A', 'A', 0.0],
         ['C', 'A', 'B', 1.0],
         ['C', 'A', 'C', 0.0],
         ['C', 'B', 'A', 1.0],
         ['C', 'B', 'B', 0.0],
         ['C', 'B', 'C', 0.0],
         ['C', 'C', 'A', 0.5],
         ['C', 'C', 'B', 0.5],
         ['C', 'C', 'C', 0.0]], [guest, prize])

s1 = pom.Node(guest, name="guest")
s2 = pom.Node(prize, name="prize")
s3 = pom.Node(monty, name="monty")


model = pom.BayesianNetwork("Monty Hall Problem")
model.add_states(s1, s2, s3)
model.add_edge(s1, s3)
model.add_edge(s2, s3)
model.bake()

print(model.probability(['A', 'A', 'B']))



"""
Student Bayesian Network: Koller Book example
"""

model_student_BN = pom.BayesianNetwork("Student Bayesian Network")


difficulty = pom.DiscreteDistribution({'D0': 0.6, 'D1': 0.4})
intelligence = pom.DiscreteDistribution({'I0': 0.7, 'I1': 0.3})
grade = pom.ConditionalProbabilityTable(
        [['I0', 'D0', 'G1', 0.3],
         ['I0', 'D0', 'G2', 0.4],
         ['I0', 'D0', 'G3', 0.3],

         ['I0', 'D1', 'G1', 0.05],
         ['I0', 'D1', 'G2', 0.25],
         ['I0', 'D1', 'G3', 0.7],

         ['I1', 'D0', 'G1', 0.9],
         ['I1', 'D0', 'G2', 0.08],
         ['I1', 'D0', 'G3', 0.02],

         ['I1', 'D1', 'G1', 0.5],
         ['I1', 'D1', 'G2', 0.3],
         ['I1', 'D1', 'G3', 0.2]], [intelligence, difficulty])

SAT = pom.ConditionalProbabilityTable([['I0', 'S0', 0.95],
                                       ['I0', 'S1', 0.05],
                                       ['I1', 'S0', 0.2],
                                       ['I1', 'S1', 0.8]], [intelligence])

letter = pom.ConditionalProbabilityTable([['G1', 'L0', 0.1],
                                          ['G1', 'L1', 0.9],

                                          ['G2', 'L0', 0.4],
                                          ['G2', 'L1', 0.6],

                                          ['G3', 'L0', 0.99],
                                          ['G3', 'L1', 0.01]], [grade])


# creating nodes
D = pom.Node(difficulty, name="Difficulty")
I = pom.Node(intelligence, name="Intelligence")
G = pom.Node(grade, name="Grade")
S = pom.Node(SAT, name="SAT")
L = pom.Node(letter, name="Letter")

# adding all nodes
model_student_BN.add_nodes(D, I, G, S, L)
# adding all edges
model_student_BN.add_edge(D, G)
model_student_BN.add_edge(I, G)
model_student_BN.add_edge(I, S)
model_student_BN.add_edge(G, L)


model_student_BN.bake()

# Book Page 54
print(model_student_BN.probability(['D0', 'I1', 'G2', 'S1', 'L0']))


# model_student_BN.plot()  # plot is not working  ## TODO Define a plot function and conversion to Gephi Format function
G = nx.MultiDiGraph()
for state in model_student_BN.states:
    # print(state.name)
    G.add_node(state.name)

for parent, child in model_student_BN.edges:
    G.add_edge(parent.name, child.name)

nx.draw_networkx(G, arrows=True, with_labels=True)

